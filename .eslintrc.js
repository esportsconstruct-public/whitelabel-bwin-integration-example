"use strict";

module.exports = {
    extends: [
        "@esportsconstruct-public",
        "prettier"
    ],
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ["./tsconfig.eslint.json"]
    },
    overrides: [
        {
            files: ["*.ts"],
            parser: "@typescript-eslint/parser",
            parserOptions: {
                sourceType: "module"
            },
            plugins: ["@typescript-eslint"],
            rules: {
                "@typescript-eslint/no-unnecessary-condition": "off", // false-positive with ? for class field
                "@typescript-eslint/no-unsafe-member-access": "off",
                "@typescript-eslint/no-empty-function": "off"
            }
        },
        {
            files: ["*.component.ts"],
            rules: {
                "class-methods-use-this": "off"
            }
        },
        {
            files: ["*.pipe.ts"],
            rules: {
                "class-methods-use-this": "off"
            }
        },
        {
            files: ["*.spec.ts"],
            rules: {
                "max-classes-per-file": "off",
                "@angular-eslint/prefer-on-push-component-change-detection": "off",
                "@angular-eslint/use-component-selector": "off",
                "@typescript-eslint/no-unused-vars-experimental": "off",
                "@typescript-eslint/no-unsafe-call": "off",
                "@typescript-eslint/no-unused-vars": "off",
                "@typescript-eslint/no-unsafe-assignment": "off",
                "@typescript-eslint/no-unsafe-member-access": "off",
                "class-methods-use-this": "off",
                "@typescript-eslint/no-explicit-any": "off",
                "@typescript-eslint/no-empty-function": "off",
                "@typescript-eslint/require-await": "off",
                "@typescript-eslint/no-floating-promises": "off"
            }
        },
        {
            files: ["projects/src/**/*.ts"],
            rules: {
                "@angular-eslint/use-injectable-provided-in": "off"
            }
        }
    ]
};
