import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
   public ngOnInit(): void {

       this.loadScript(
           "https://staging.bwinesport.website/scripts/esportsbook-iframe.js",
           document.getElementById("esportsbook")
       ).then((status) => {
           if (status) {
              this.scriptLoaded();
           }
       });

   }

   private scriptLoaded(): void {
       /**
        *  Implement the get current lang code here.
        *  Example: var currentLang = window.location.pathname.split("/")[1] + "-xx";
        *  We accept any second part code after a hyphen. "en-xx" - allowed
        * @type {string}
        */
       var currentLang = "en-gb";

       /**
        * This variable contains the HtmlElement with the maximum window width
        * Change to get the container element
        * @type {HTMLElement}
        */
       var esportsbookElement = document.getElementById("esportsbook");

       if (esportsbookElement) {
           var esportsbookBuilder = new EsportsbookBuilder(esportsbookElement, {
               codeName: "gvc",
               appUrl: "https://staging.bwinesport.website",
               router: {
                   skipSectionsCountTo: 0, // Example: https://test.esports.bwin.com/en is skipSectionsCountTo: 1
                   langCode: currentLang,
                   hideParams: {
                       brandid: "BWINCOM",
                       frontendid: "bz",
                       productid: "SPORTSBOOK",
                       parent: "test.esports.bwin.com"
                   }
               },
               panel: {
                   jsUrl:
                       "https://staging.bwinesport.website/elements/esportsbook-panel-webcomponent.js",
                   assetsUrl: "https://staging.bwinesport.website/assets-panel",
                   assetsUrlCommon: "https://staging.bwinesport.website/assets-common",
                   theme: "bwin",
                   injectTo: esportsbookElement
               },
               callbacks: {
                   /**
                    * This callback will be triggered when the user clicks the login button in our application.
                    * This should redirect to your login page or open a modal or whatever.
                    */
                   clickToLogin: () => {
                       const url = new URL(location.href);
                       const searchParams = new URLSearchParams(url.search);
                       if (!searchParams.has("token")) {
                           searchParams.append("token", "any");
                           window.location.replace(
                               `${window.location.protocol}//${window.location.host}${
                                   window.location.pathname
                               }?${searchParams.toString()}`
                           );
                       }
                   },
                   /**
                    * Callback of the loaded panel, it's time to restore the session if it was
                    */
                   panelLoaded: () => {
                       setTimeout(() => {
                           const url = new URL(location.href);
                           const searchParams = new URLSearchParams(url.search);
                           const tokenFromUrl = searchParams.get("token");
                           if (tokenFromUrl && esportsbookBuilder.esportsbook) {
                               const token = tokenFromUrl;
                               esportsbookBuilder.esportsbook.sendLogin(
                                   new Date().toISOString(),
                                   new Date(new Date().getTime() + 60000).toISOString(),
                                   token
                               );
                           }
                       }, 100);
                   },
                   /**
                    * A few seconds before the expirationTime passes. We call this callback to get a new token and new expiration dates
                    * @returns {Promise<unknown>}
                    */
                   sendExtendUserSession: () => {
                       return new Promise((resolve, reject) => {
                           let token = "test";

                           const url = new URL(location.href);
                           const searchParams = new URLSearchParams(url.search);
                           const tokenFromUrl = searchParams.get("token");
                           if (tokenFromUrl) {
                               token = tokenFromUrl;
                           }
                           resolve({
                               creationTime: new Date().toISOString(),
                               expirationTime: new Date(
                                   new Date().getTime() + 60000
                               ).toISOString(),
                               sessionToken: token
                           });
                       });
                   },
                   /**
                    * Implement the language change logic (from esportsbook) here
                    * We have a select with a choice of language inside app, maybe you just replace it in your url, maybe you will do something more complex
                    * @param langCode
                    */
                   changeLangCode: (langCode) => {
                       currentLang = langCode;
                   }
               }
           });

           /**
            * You can call this when your page language changes without reloading
            */
           esportsbookBuilder.esportsbook?.changeLangCode(currentLang);

           /**
            * Used to correctly position absolute elements within a frame. Must be called at least once
            * @param {number} headerHeight
            * @param {number} footerHeight
            * @param {number} headerFixedHeight
            * @param {number} footerFixedHeight
            */
           esportsbookBuilder.esportsbook?.changeHeaderFooterSizes(
               document.getElementsByClassName("header")[0].clientHeight,
               document.getElementsByClassName("footer-wrapper")[0].clientHeight,

               document.getElementsByClassName("header")[0].clientHeight,
               0
           );
       }
   }

    private async loadScript(src: string, injectTo?: HTMLElement | null): Promise<boolean> {
        return new Promise((resolve) => {
            let alreadyLoaded = false;

            const scripts = document.getElementsByTagName("script");

            // eslint-disable-next-line @typescript-eslint/prefer-for-of
            for (let i = 0; i < scripts.length; ++i) {
                if (scripts[i].getAttribute("src") === src) {
                    alreadyLoaded = true;
                }
            }

            if (!alreadyLoaded) {
                const script = document.createElement("script");
                script.type = "text/javascript";
                script.async = false;

                // script.noModule = false;
                // script.defer = true;
                script.src = src;
                script.onload = (): void => {
                    resolve(true);
                };
                script.onerror = (): void => {
                    resolve(false);
                };
                if (injectTo) {
                    injectTo.appendChild(script);
                } else {
                    document.getElementsByTagName("body")[0].appendChild(script)
                }
            } else {
                resolve(false);
            }
        });
    }
}
