/* eslint-disable */
// Global constants (defined with webpack).

// declare const esportsbookElement: string;

declare class BetslipsPanel {
    protected betslipsPanelElement: HTMLElement | null;
    private paddingTop;
    private paddingBottom;
    private headerHeight;
    private headerFixedHeight;
    private footerHeight;
    private footerFixedHeight;
    private opened;
    private isMobile;
    private clientHeight;
    private esportsbookHeight;
    /**
     * Example:
     *    var betslipsPanel = new BetslipsPanel(document.getElementById("esportsbook-betslips-panel-iframe"));
     * @param {HTMLIFrameElement} betslipsPanelElement
     */
    constructor(betslipsPanelElement: HTMLElement | null);
    /**
     * Update dimension variables and recalculate panel position
     * @param {number} clientHeight - Better value Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
     * @param {number} esportsbookHeight - esportsbook iframe height
     * @param {number} headerHeight
     * @param {number} footerHeight
     * @param {number} headerFixedHeight
     * @param {number} footerFixedHeight
     */
    updateSizes(
        clientHeight: number,
        esportsbookHeight: number,
        headerHeight: number,
        footerHeight: number,
        headerFixedHeight: number,
        footerFixedHeight: number
    ): void;
    /**
     * Open or close the panel. Also used when resizing footer and header
     * @param {boolean} opened
     */
    setStyles(opened: boolean): void;
    private updatePaddings;
}
declare class BetslipsPanelFrame extends BetslipsPanel {
    constructor(betslipsPanelFrameElement: HTMLIFrameElement | null);
    private redirectMessageToFrame;
}
interface EsportsbookConfig {
    codeName: string;
    router?: {
        skipSectionsCountTo: number;
        langCode?: string;
        hideParams?: {
            [key: string]: string;
        };
    };
    callbacks?: {
        panelLoaded?: () => void;
        clickToLogin?: () => void;
        sendExtendUserSession?: () => Promise<{
            creationTime: string;
            expirationTime: string;
            sessionToken: string;
        }>;
        changeLangCode?: (langCode: string) => void;
    };
}
declare class Esportsbook {
    private loadedCallback?;
    private esportsbookFrameElement;
    private iframeHeight;
    private iframeMinHeight;
    private headerHeight;
    private footerHeight;
    private headerFixedHeight;
    private footerFixedHeight;
    private readonly token;
    private panel?;
    private config;
    private currentLandCode?;
    /**
     * @param {HTMLIFrameElement} esportsbookFrameElement
     * @param {string} windowName - The code word is transmitted to the client along with the environment. Required to filter messages
     * @param {BetslipsPanel} panel - Optional parameter, this is an instance of the panel class, if the client uses our panel
     */
    constructor(
        esportsbookFrameElement: HTMLIFrameElement | null,
        config: EsportsbookConfig,
        panel?: BetslipsPanel,
        loadedCallback?: (uniqueCode: string) => void
    );
    setPanel(panel: BetslipsPanel): void;
    /**
     * Used to correctly position absolute elements within a frame. Must be called at least once
     * @param {number} headerHeight
     * @param {number} footerHeight
     * @param {number} headerFixedHeight
     * @param {number} footerFixedHeight
     */
    changeHeaderFooterSizes(
        headerHeight: number,
        footerHeight: number,
        headerFixedHeight: number,
        footerFixedHeight: number
    ): void;
    sendLogin(creationTime: string, expirationTime: string, sessionToken: string): void;
    sendLogout(): void;
    changeLangCode(langCode: string): void;
    private sendSizes;
    private getMessageFromIframe;
    private cutLeftUrl;
    private cutRightUrl;
    private sendExtendUserSession;
}
interface EsportsbookBuilderConfig extends EsportsbookConfig {
    appUrl: string;
    panel: {
        url?: string;
        jsUrl?: string;
        assetsUrl?: string;
        assetsUrlCommon?: string;
        theme?: string;
        injectTo?: HTMLElement | null;
    };
}
declare class EsportsbookBuilder {
    esportsbook?: Esportsbook;
    private readonly container;
    private betslipsPanel?;
    private betslipsPanelElement?;
    private esportsbookElement?;
    private readonly config;
    constructor(container: HTMLElement | null, config: EsportsbookBuilderConfig);
    private createElements;
    private setAppFrameStyles;
    private setPanelStyles;
    private createFrame;
    private createPanelFromWebcomponent;
    private loadScript;
}
